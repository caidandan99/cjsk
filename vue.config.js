const webpack = require('webpack')
module.exports = {
    transpileDependencies: ['webpack-dev-server/client'],
    chainWebpack: config => {
        config.entry.app = ['babel-polyfill', './src/main.js'];
    },
    publicPath:"./",
    outputDir:"dist",
    productionSourceMap: false,
    assetsDir:"assets",
    devServer: {
        host:"cjsk.yaowan.com",//要设置当前访问的ip 否则失效
        // host:"www.zq.com",//要设置当前访问的ip 否则失效
        // host:"test-cjsk.yaowan.com",//要设置当前访问的ip 否则失效
        port: 8080,
        // open: true, //浏览器自动打开页面
        proxy: {
            '/api': {
                // target: 'https://test-ywcms.allrace.com',
                target: 'https://ywcms.allrace.com/api/d7aqtf3gxy',
                // target: 'https://www.easy-mock.com/mock/5cc6bd7aef1b36094e8d9b21/cjsk',
                ws: true,
                changeOrigin: true,
                pathRewrite:{
                    '^/api':''
                }
            }
        }
    },
    configureWebpack: {

        plugins: [

            new webpack.ProvidePlugin({

                $:"jquery",

                jQuery:"jquery",

                "windows.jQuery":"jquery"

            })

        ]

    }
}