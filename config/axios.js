import {
    baseUrl, //引入baseUrl
} from "../config/env";
import axios from 'axios';
import Qs from 'qs'; //序列化post参数
axios.defaults.timeout = 10000; //设置请求时间
axios.defaults.baseURL = baseUrl;//设置默认接口地址
axios.defaults.withCredentials=true;
/**
 * 封装get方法
 * @param url
 * @param data
 * @returns {Promise}
 */
export function fetch(url,params={}){
    return new Promise((resolve,reject) => {
        axios.get(url,{
            params:params
        })
            .then(response => {
                resolve(response.data);
            })
            .catch(err => {
                reject(err)
            })
    })
}
/**
 * 封装post请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function post(url,data = {}){
    return new Promise((resolve,reject) => {
        axios.post(url, Qs.stringify(data), {
            headers:{'Content-Type':'application/x-www-form-urlencoded'},
            withCredentials : true
        })
            .then(response => {
                resolve(response.data);
            },err => {
                reject(err)
            })
    })
}