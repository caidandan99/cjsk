import Vue from 'vue'
import Router from 'vue-router'
// import Home from './views/Home.vue'
import Index from '@/components/Index'
import GongLue from '@/components/GongLue'
// import Test from '@/components/Test'
import News from '@/components/News'
import Detail from '@/components/Detail'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: '/',
            component: Index
        },
        {
            path: '/index',
            name: 'index',
            component: Index
        },
        {
            path: '/gonglue',
            name: 'gonglue',
            component: GongLue
        },
        {
            path: '/news',
            name: 'news',
            component: News
        },
        {
            path: '/detail',
            name: 'detail',
            component: Detail
        },
    ],

})
