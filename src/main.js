import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store'

//解决低版本白屏问题
// import '@babel/polyfill';
// import Es6Promise from 'es6-promise'
// Es6Promise.polyfill()

import VueAwesomeSwiper from 'vue-awesome-swiper'
Vue.use(VueAwesomeSwiper, /* { default global options } */)
//图表引入
import echarts from "echarts"
Vue.prototype.$echarts = echarts

import filterTime from './assets/js/common'
// 注册为全局过滤器
Vue.filter('filterTime', filterTime)
Vue.config.productionTip = false

//引入clipboard
import clipboard from 'clipboard';
Vue.prototype.clipboard = clipboard;

/**
 * 访问API
 */
import Axios from 'axios';
import {post, fetch} from "../config/axios";
Vue.prototype.$http = Axios
Vue.prototype.$get = fetch;
Vue.prototype.$post = post;

//引入全局配置变量
import global_ from './components/Global'//引用文件
Vue.prototype.GLOBAL = global_//挂载到Vue实例上面

//定义全局函数 上报浏览栏目数据
Vue.prototype.reportCatData = function (cat_id) {
    // var api = global_.SITE_DOMAIN + '/api/' + global_.SITE_NO;
    post("/catalog/" + cat_id + "/stat", {
        type: 1, //浏览
    }).then((data) => {
        if (data.code == 200) {
            console.info('上报栏目成功' + cat_id);
        }
    });
}

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
