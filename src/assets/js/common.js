var filterTime = function (filter_time) {
    var date = filter_time.toString();
    date = date.substring(0, 19);
    date = date.replace(/-/g, '/');
    var timestamp = new Date(date).getTime();
    var date = new Date(timestamp);
    var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
    var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
    var tips = month + "-" + day;
    return tips;
}
export default filterTime